console.log("Hello World!");
// [SECTION] WHILE LOOP - expression acts as conditions, iteration-repetition of statement
// SYNTAX while(expression/condition){statement}

let count = 5;
while(count !== 0){
	console.log("While: " + count);
	count--;    
}


// Do-While Loop
// do{statement}
// while(expression/condition)

let number = Number(prompt("Give me a number"));
do{
	// the cureent value of number is printed out
	console.log("Do While: " + number);
	// increases the number by 1 after every iteration to stop the loop when it reaches 10 or greater
	number += 1;
	// number = number + 1;
} while(number <= 10);


// [SECTION] For Loops - IMPORTANT
// 3 parts: 
// SYNTAX: for (initialization; expression/condition; finalExpression){statement}
for (let count = 0; count <= 20; count++){
	console.log(count);
}

for (let count = 0; count <= 33; count++){
	console.log(count);
}


let myString = "Alex";
console.log(myString.length);
// counts the number of the strings

console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[-1]);
// -1 - locating the last letter, array

for(let x = 0; x < myString.length; x++){
	console.log(myString[x]);
}



let myName = "Pepita";
console.log(myName.length);
for(let p = 0; p < myName.length; p++){
	// console.log(myName[p].toLowerCase());

	if(
		myName[p].toLowerCase() == "a" ||
		myName[p].toLowerCase() == "i" ||
		myName[p].toLowerCase() == "e" ||
		myName[p].toLowerCase() == "o" ||
		myName[p].toLowerCase() == "u" 
		){
		// if letter is a vowel, it will print 3
		console.log(3)
	}else{
		console.log(myName[p]);
	}
}


// [SECTION] Continue and break statements
for (let count = 0; count <= 20; count++){
	if (count % 2 === 0){
		continue; 
	}
	console.log("Continue and break: " + count);

	if (count > 10){
		break;
	}
}




let name = "alexandro";
for (let i = 0; i < name.length; i++){
	console.log(name[i]);

	if (name[i].toLowerCase() === "a"){
		console.log("Continue to the next iteration");
		continue;
	}
	if (name[i] == "d") {
		break;
	}
}